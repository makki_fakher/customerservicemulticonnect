package com.youssfi.customerservice.mapper;

import com.youssfi.customerservice.dto.CustomerRequest;
import com.youssfi.customerservice.entities.Customer;
import com.youssfi.customerservice.stub.CustomerServiceOuterClass;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CustomerMapper {

    private ModelMapper modelMapper =new ModelMapper();

    public Customer fromCustomerRequest(CustomerRequest customerRequest) {
        Customer customer = modelMapper.map(customerRequest, Customer.class);
        return customer;
    }

    //convertir un objet customer vers un object customer GRPC
    // il faut utiliser les builders pour la conversion
    public CustomerServiceOuterClass.Customer fromCustomer(Customer customer) {
        return modelMapper.map(customer, CustomerServiceOuterClass.Customer.Builder.class).build();
    }

    public Customer fromGRPCCustomerRequest(CustomerServiceOuterClass.CustomerRequest customerRequest) {
        return modelMapper.map(customerRequest, Customer.class);
    }


}

package com.youssfi.customerservice.web;

import com.youssfi.customerservice.entities.Customer;
import com.youssfi.customerservice.mapper.CustomerMapper;
import com.youssfi.customerservice.repository.CustomerRepository;
import com.youssfi.customerservice.stub.CustomerServiceGrpc;
import com.youssfi.customerservice.stub.CustomerServiceOuterClass;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

@GrpcService
public class CustomerGrpcService extends CustomerServiceGrpc.CustomerServiceImplBase {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private CustomerMapper customerMapper;
    @Override
    public void getAllCustomers(CustomerServiceOuterClass.GetAllCustomersRequest request, StreamObserver<CustomerServiceOuterClass.GetCustomersResponse> responseObserver) {
        List<Customer> customerList = customerRepository.findAll();
        List<CustomerServiceOuterClass.Customer> grpcCustomers = customerList
                .stream()
                .map(customerMapper::fromCustomer)
                .collect(Collectors.toList());

        CustomerServiceOuterClass.GetCustomersResponse customersResponse = CustomerServiceOuterClass.GetCustomersResponse
                .newBuilder()
                .addAllCustomers(grpcCustomers)
                .build();

        responseObserver.onNext(customersResponse);
        responseObserver.onCompleted();


    }

    @Override
    public void getCustomerById(CustomerServiceOuterClass.GetCustomerByIdRequest request, StreamObserver<CustomerServiceOuterClass.GetCustomersByIdResponse> responseObserver) {

        Customer customerEntity = customerRepository.findById(request.getCustomerId()).get();

        CustomerServiceOuterClass.GetCustomersByIdResponse response =
                CustomerServiceOuterClass.GetCustomersByIdResponse.newBuilder()
                        .setCustomer(customerMapper.fromCustomer(customerEntity))
                        .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();

    }

    @Override
    public void saveCustomer(CustomerServiceOuterClass.SaveCustomerRequest request, StreamObserver<CustomerServiceOuterClass.SaveCustomerResponse> responseObserver) {

        CustomerServiceOuterClass.CustomerRequest customerRequest = request.getCustomer();
        Customer customer = customerMapper.fromGRPCCustomerRequest(customerRequest);
        Customer savedCustomer = customerRepository.save(customer);

        CustomerServiceOuterClass.SaveCustomerResponse resp = CustomerServiceOuterClass.SaveCustomerResponse.newBuilder()
                .setCustomer(customerMapper.fromCustomer(savedCustomer)).build();

        responseObserver.onNext(resp);
        responseObserver.onCompleted();


    }
}

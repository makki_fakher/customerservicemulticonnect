package com.youssfi.customerservice.web;

import com.youssfi.customerservice.entities.Customer;
import com.youssfi.customerservice.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@AllArgsConstructor
public class CustomerRestController {
    private CustomerRepository customerrepo;

    @GetMapping("/customers")
    public List<Customer> customerListe(){
        return customerrepo.findAll();

    }

    @GetMapping("/customer/{id}")
    public Customer customerListe(@PathVariable Long id){
        return customerrepo.findById(id).get();
    }

    @PostMapping("/saveCustomer")
    public Customer saveCustomer(@RequestBody Customer customer) {
        return customerrepo.save(customer);

    }

}

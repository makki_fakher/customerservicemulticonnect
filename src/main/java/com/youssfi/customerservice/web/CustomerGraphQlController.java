package com.youssfi.customerservice.web;

import com.youssfi.customerservice.dto.CustomerRequest;
import com.youssfi.customerservice.entities.Customer;
import com.youssfi.customerservice.mapper.CustomerMapper;
import com.youssfi.customerservice.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
@AllArgsConstructor
public class CustomerGraphQlController {
    private CustomerRepository customerrepo;

    private CustomerMapper customerMapper;

    @QueryMapping
    public List<Customer> allCustomer(){
        return customerrepo.findAll();
    }

    @QueryMapping
    public Customer customerById(@Argument Long id){
        Customer customer =  customerrepo.findById(id).orElse(null);
        if (customer == null) throw new RuntimeException(String.format("customer not found"));
        return customer;
    }


    @MutationMapping
    public Customer saveCustomer(@Argument CustomerRequest customer){

        Customer c = customerMapper.fromCustomerRequest(customer);
        return customerrepo.save(c);
    }
}

package com.youssfi.customerservice;

import com.youssfi.customerservice.entities.Customer;
import com.youssfi.customerservice.repository.CustomerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CustomerServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CustomerServiceApplication.class, args);
    }

    @Bean
    CommandLineRunner start(CustomerRepository customer){
        return args -> {
            customer.save(Customer.builder()
                    .name("fakher")
                    .email("fakher@gmail.com")
                    .build()
            );

            customer.save(Customer.builder()
                    .name("mekki")
                    .email("mekki@gmail.com")
                    .build()
            );

            customer.save(Customer.builder()
                    .name("test")
                    .email("test@gmail.com")
                    .build()
            );

        };

    }
}

package com.youssfi.customerservice.service;

import com.youssfi.customerservice.dto.CustomerRequest;
import com.youssfi.customerservice.entities.Customer;
import com.youssfi.customerservice.mapper.CustomerMapper;
import com.youssfi.customerservice.repository.CustomerRepository;
import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
@WebService(serviceName = "CustomerWS")
public class CustomerSoapService {
    private CustomerRepository customerRepository;
    private CustomerMapper customerMapper;

    @WebMethod
    public List<Customer> customerList(){
        return customerRepository.findAll();

    }
    @WebMethod
    public Customer customerById(@WebParam(name = "id") Long id){
        return customerRepository.findById(id).get();
        
    }

    public Customer saveCustomer(@WebParam(name = "customer") CustomerRequest customer){
        Customer c = customerMapper.fromCustomerRequest(customer);
        return customerRepository.save(c);

    }
}
